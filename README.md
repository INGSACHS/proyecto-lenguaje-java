PROYECTO LENGUAJE JAVA
======================

Descripción del proyecto
------------------------

Proyecto creado en lenguaje java para dar solución a la prueba técnica de la 
empresa ZINOBE para aplicar al cargo de analista de pruebas

Estructura del proyecto
-----------------------

El proyecto fue creado como tipo maven y emplea el patrón de diseño POM para la
automatización de casos de prueba. Para ello, se han creado las siguientes 
carpetas dentro de la ruta "src\test\java":

- Pages
- Test

Dentro de la carpeta Pages se encuentran las siguientes clases:

- BaseTestSuite
- PagePrincipal

Dentro de la clase "BaseTestSuite" se encuentra la lógica para acceder a los
método generales para interactuar con el navegador y dentro de la clase 
"PagePrincipal" se encuentra los objetos y métodos para interactuar con la
página web.

Dentro de la carpeta Test se encuentra las siguientes clases:

- SimuladorCredito
- ValidarEnlaces

Dentro de la clase "SimuladorCredito" se encuentran diseñados los casos de
prueba orientados a validar el simulador del crédito y dentro de la clase
"ValidarEnlaces" se encuentra diseñado el caso de prueba orientado a validar
los diferentes enlaces y el botón Mi Cuenta de la página principal.

Ejecutar casos de prueba
------------------------

1. Abrir el programa Intellij IDEA
2. Seleccionar la opción "Open or Import"
3. Buscar la ruta donde se encuentra el proyecto y seleccionar el archivo
"pom.xml" y hacer clic en el botón "OK"
4. Hacer clic en el botón "Open as Project"
5. Una vez cargado el proyecto, buscar el archivo "TestSuite.xml" y realiza clic
derecho y seleccionar la opción "Run"