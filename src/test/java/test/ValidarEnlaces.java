package test;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.BaseTestSuite;
import java.io.IOException;

public class ValidarEnlaces extends BaseTestSuite {

    //Declaración de atributos
    String tituloPaginaQuienesSomos, tituloPaginaComoFunciona, tituloPaginaBeneficios, tituloPaginaAyuda,
    labelIniciarSesion;

    @Test(description = "TC_001", priority = 1)
    public void validarEnlacesPantallaPrincipal() throws InterruptedException, IOException {

        //Esperar que la página carge por completo
        carguePaginaCompleta(driver);

        //Abrir enlace "Quíenes Somos" y obtener el texto del encabezado
        tituloPaginaQuienesSomos=pagePrincipal.validarLinkQuinesSomos();
        tomarCapturaPantalla(driver,"C:/selenium/TC_003_Enlace Quienes Somos.png");
        System.out.println(tituloPaginaQuienesSomos);
        espera(5);

        //Abrir enlace "Cómo Funciona" y obtener el texto del encabezado
        tituloPaginaComoFunciona=pagePrincipal.validarLinkComoFunciona();
        tomarCapturaPantalla(driver,"C:/selenium/TC_003_Enlace Como Funciona.png");
        System.out.println(tituloPaginaComoFunciona);
        espera(5);

        //Abrir enlace "Beneficios" y obtener el texto del encabezado
        tituloPaginaBeneficios=pagePrincipal.validarLinkBeneficios();
        tomarCapturaPantalla(driver,"C:/selenium/TC_003_Enlace Beneficios.png");
        System.out.println(tituloPaginaBeneficios);
        espera(5);

        //Abrir enlace "Ayuda" y obtener el texto del encabezado
        tituloPaginaAyuda=pagePrincipal.validarLinkAyuda();
        tomarCapturaPantalla(driver,"C:/selenium/TC_003_Enlace Ayuda.png");
        System.out.println(tituloPaginaAyuda);
        espera(5);

        //Hacer clic botón "Mi Cuenta" y obtener el texto del encabezado
        labelIniciarSesion=pagePrincipal.validarBotonMiCuenta();
        tomarCapturaPantalla(driver,"C:/selenium/TC_003_Iniciar Sesion.png");
        System.out.println(labelIniciarSesion);

        //Bloque para validar los encabezados obtenidos en cada página
        Assert.assertTrue(tituloPaginaQuienesSomos.equals("¡HOLA! SOMOS LINERU"),"El titulo obtenido no corresponde" +
                "con el mensaje que se debe de mostrar para el enlace \"Quíenes Somos\"");
        Assert.assertTrue(tituloPaginaComoFunciona.equals("CRÉDITOS PARA TU TRANQUILIDAD"),"El titulo obtenido no corresponde" +
                "con el mensaje que se debe de mostrar para el enlace \"Cómo Funciona\"");
        Assert.assertTrue(tituloPaginaBeneficios.equals("PENSAMOS EN TI"),"El titulo obtenido no corresponde" +
                "con el mensaje que se debe de mostrar para el enlace \"Beneficios\"");
        Assert.assertTrue(tituloPaginaAyuda.equals("ACLARA TUS DUDAS"),"El titulo obtenido no corresponde" +
                "con el mensaje que se debe de mostrar para el enlace \"Ayuda\"");
        Assert.assertTrue(labelIniciarSesion.equals("INICIA SESIÓN EN LINERU"),"El titulo obtenido no corresponde" +
                "con el mensaje que se debe de mostrar en el formulario de \"Iniciar Sesión\"");

    }
}
