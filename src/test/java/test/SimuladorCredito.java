package test;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.BaseTestSuite;

import java.io.IOException;
import java.util.List;

public class SimuladorCredito extends BaseTestSuite {

    //Declaración de atributos
    int monto;

    @Test(description = "TC_001", priority = 1)
    public void simularCreditoMayor() throws InterruptedException, IOException {

        //Esperar que la página carge por completo
        carguePaginaCompleta(driver);

        //Setear el valor del monto y llamar al método que ingresa el monto
        monto = 850000;
        pagePrincipal.ingresarMonto(monto);
        espera(2);

        //Realizar una captura de pantalla
        tomarCapturaPantalla(driver,"C:/selenium/TC_001_Simular Crédito.png");

        //Llamar al método encargado de obtener los resultados de la simulación del crédito y el texto de la alerta
        List<Integer> resultados = pagePrincipal.obtenerResultadoCredito();
        String mensaje = pagePrincipal.validarLabel();

        //Bloque encargado de validar los valores obtenidos de la simulación del crédito y el texto de la alerta
        Assert.assertTrue(monto==resultados.get(0),"El valor del tag \"Valor Solicitado\" del " +
                "simulador es diferente al monto enviado como parámetro a la calculadora de crédito");

        Assert.assertTrue(resultados.get(3).equals(resultados.get(1)),"El valor del tag \"Sub Total\" " +
                "es diferente al valor del subtotal calculado de la suma de los tags \"Valor Solicitado\", " +
                "\"Interés\", \"Seguro\" y \"Administración\"");

        Assert.assertTrue(resultados.get(4).equals(resultados.get(2)),"El valor del tag \"TOTAL A PAGAR\" " +
                "es diferente al valor del total a pagar calculado de la suma de los tags \"Sub Total\", " +
                "\"Tecnología\" y \"IVA\"");

        Assert.assertTrue(mensaje.contains("Sí es tu primera vez"),"El mensaje obtenido no corresponde al " +
                "mensaje esperado cuando se solicita un crédito por primera vez");

    }

    @Test(description = "TC_002", priority = 2)
    public void simularCreditoMenor() throws InterruptedException, IOException {

        //Esperar que la página carge por completo
        carguePaginaCompleta(driver);

        //Setear el valor del monto y llamar al método que ingresa el monto
        monto = 330000;
        pagePrincipal.ingresarMonto(monto);
        espera(2);

        //Realizar una captura de pantalla
        tomarCapturaPantalla(driver,"C:/selenium/TC_002_Simular Crédito.png");

        //Llamar al método encargado de obtener los resultados de la simulación del crédito
        List<Integer> resultados = pagePrincipal.obtenerResultadoCredito();

        //Bloque encargado de validar los valores obtenidos de la simulación del crédito
        Assert.assertTrue(monto==resultados.get(0),"El valor del tag \"Valor Solicitado\" del " +
                "simulador es diferente al monto enviado como parámetro a la calculadora de crédito");

        Assert.assertTrue(resultados.get(3).equals(resultados.get(1)),"El valor del tag \"Sub Total\" " +
                "es diferente al valor del subtotal calculado de la suma de los tags \"Valor Solicitado\", " +
                "\"Interés\", \"Seguro\" y \"Administración\"");

        Assert.assertTrue(resultados.get(4).equals(resultados.get(2)),"El valor del tag \"TOTAL A PAGAR\" " +
                "es diferente al valor del total a pagar calculado de la suma de los tags \"Sub Total\", " +
                "\"Tecnología\" y \"IVA\"");

    }

}
