package pages;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BaseTestSuite {

    //Declaración de atributos
    protected WebDriver driver;
    protected PagePrincipal pagePrincipal;
    private JavascriptExecutor js;
    private String estado;

    //Método encargado de inicializar las paginas que contienen los objetos
    public void inicializarPaginas(WebDriver driver){
        pagePrincipal = new PagePrincipal(driver);
    }

    //Método encargado de validar si la página cargo en su totalidad
    public void carguePaginaCompleta(WebDriver driver){
        js = (JavascriptExecutor)driver;
        do {
            estado = (String)js.executeScript("return document.readyState");
        }while (!estado.equals("complete"));
    }

    //Método encargado de generar una espera dentro de la ejecución, recibe como parámetro el tiempo en segundos
    public void espera(int segundos) throws InterruptedException {
        Thread.sleep(segundos*1000);
    }

    //Método encargado de tomar una captura de pantalla, recibe como parámetros el driver y la ruta donde
    //se alojara la imagen
    public void tomarCapturaPantalla(WebDriver driver, String fileWithPath) throws IOException {
        TakesScreenshot scrShot = (TakesScreenshot)driver;
        File srcFile = scrShot.getScreenshotAs(OutputType.FILE);
        File destFile = new File(fileWithPath);
        FileUtils.copyFile(srcFile,destFile);
    }

    //Método encargado de abrir el navegador, se ejecuta antes de cada prueba
    @BeforeMethod
    public void abrirNavegador(){
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        inicializarPaginas(driver);
        driver.get("https://www.lineru.com/");
    }

    //Método encargado de cerrar el navegador, se ejecuta luego de cada prueba
    @AfterMethod
    public void cerrarNavegador(){
        driver.quit();
    }

}
