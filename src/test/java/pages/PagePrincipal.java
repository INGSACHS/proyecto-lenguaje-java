package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class PagePrincipal {

    //Declaración de atributos
    private WebDriver driver;
    private WebDriverWait wait;
    private int subtotalCalculado, totalPagarCalculado;
    private List<Integer> resultadosObtenido;

    //Creación e identificación de elementos web
    @FindBy(css = "#mat-input-0")
    WebElement inputMonto;

    @FindBy(xpath = "//div[@class='alert__content']")
    WebElement labelPrimeraVez;

    @FindBy(xpath = "(//li[@class='clearfix mb-1']//div[2]//strong)[1]")
    WebElement labelValorSolicitado;

    @FindBy(xpath = "(//li[@class='clearfix mb-1']//div[2]//span)[1]")
    WebElement labelInteres;

    @FindBy(xpath = "(//li[@class='clearfix mb-1']//div[2]//span)[2]")
    WebElement labelSeguro;

    @FindBy(xpath = "(//li[@class='clearfix']//div[2]//span)[1]")
    WebElement laberlAdministracion;

    @FindBy(xpath = "(//li[@class='clearfix mb-1']//div[2]//strong)[2]")
    WebElement labelSubtotal;

    @FindBy(xpath = "(//li[@class='clearfix mb-1']//div[2]//span)[3]")
    WebElement labelTecnologia;

    @FindBy(xpath = "(//li[@class='clearfix']//div[2]//span)[2]")
    WebElement labelIva;

    @FindBy(xpath = "//ul[@class='list-unstyled display-2']//li//div[2]//span")
    WebElement labelTotalPagar;

    @FindBy(xpath = "//span[contains(.,'Quíenes somos')]")
    WebElement linkQuienesSomos;

    @FindBy(xpath = "//span[contains(.,'Cómo funciona')]")
    WebElement linkComoFunciona;

    @FindBy(xpath = "//span[contains(.,'Beneficios')]")
    WebElement linkBeneficios;

    @FindBy(xpath = "//span[contains(.,'Ayuda')]")
    WebElement linkAyuda;

    @FindBy(xpath = "//a[@role='button'][contains(.,'Mi cuenta')]")
    WebElement botonMiCuenta;

    @FindBy(xpath = "//h1[contains(.,'¡HOLA! SOMOS LINERU')]")
    WebElement labelPaginaQuienesSomos;

    @FindBy(xpath = "//h1[contains(.,'CRÉDITOS PARA TU TRANQUILIDAD')]")
    WebElement labelPaginaComoFunciona;

    @FindBy(xpath = "//h1[contains(.,'PENSAMOS EN TI')]")
    WebElement labelPaginaBeneficios;

    @FindBy(xpath = "//h1[contains(.,'ACLARA TUS DUDAS')]")
    WebElement labelPaginaAyuda;

    @FindBy(xpath = "//h3[contains(.,'Inicia sesión en Lineru')]")
    WebElement labelBotonMiCuenta;

    //Método constructor de la clase PagePrincipal, recibe como parámetro el driver
    public PagePrincipal(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 15);
    }

    //Método encargado de ingresar el monto, recibe como parámetro el monto
    public void ingresarMonto(int monto){
        inputMonto.clear();
        inputMonto.sendKeys(String.valueOf(monto));
    }

    //Método encargado de capturar el resultado de la simulación del crédito y realizar los respectivos cálculos
    public List<Integer> obtenerResultadoCredito() {
        //Cálculo del subtotal
        subtotalCalculado = Integer.parseInt(labelValorSolicitado.getText().substring(1).replace(",",""))
                + Integer.parseInt(labelInteres.getText().substring(1).replace(",",""))
                + Integer.parseInt(labelSeguro.getText().substring(1).replace(",",""))
                + Integer.parseInt(laberlAdministracion.getText().substring(1).replace(",",""));

        //Cálculo del total a pagar
        totalPagarCalculado = subtotalCalculado
                + Integer.parseInt(labelTecnologia.getText().substring(1).replace(",",""))
                + Integer.parseInt(labelIva.getText().substring(1).replace(",",""));

        //Crear una lista con los valores recuperados y calculados y devolverla al paso de prueba
        resultadosObtenido = new ArrayList<Integer>();
        resultadosObtenido.add(0,Integer.parseInt(labelValorSolicitado.getText().substring(1).replace(",","")));
        resultadosObtenido.add(1,subtotalCalculado);
        resultadosObtenido.add(2,totalPagarCalculado);
        resultadosObtenido.add(3,Integer.parseInt(labelSubtotal.getText().substring(1).replace(",","")));
        resultadosObtenido.add(4,Integer.parseInt(labelTotalPagar.getText().substring(1).replace(",","")));

        return resultadosObtenido;
    }

    //Método que recupera y devuelve el texto de la alerta cuando se solicita un crédito mayor a $600.000
    public String validarLabel(){
        wait.until(ExpectedConditions.visibilityOf(labelPrimeraVez));
        return labelPrimeraVez.getText();
    }

    //Método que recupera y devuelve el texto del encabezado de la página "Quienes Somos"
    public String validarLinkQuinesSomos() {
        wait.until(ExpectedConditions.elementToBeClickable(linkQuienesSomos)).click();
        wait.until(ExpectedConditions.visibilityOf(labelPaginaQuienesSomos));
        return labelPaginaQuienesSomos.getText();
    }

    //Método que recupera y devuelve el texto del encabezado de la página "Cómo Funciona"
    public String validarLinkComoFunciona() {
        wait.until(ExpectedConditions.elementToBeClickable(linkComoFunciona)).click();
        wait.until(ExpectedConditions.visibilityOf(labelPaginaComoFunciona));
        return labelPaginaComoFunciona.getText();
    }

    //Método que recupera y devuelve el texto del encabezado de la página "Beneficios"
    public String validarLinkBeneficios() {
        wait.until(ExpectedConditions.elementToBeClickable(linkBeneficios)).click();
        wait.until(ExpectedConditions.visibilityOf(labelPaginaBeneficios));
        return labelPaginaBeneficios.getText();
    }

    //Método que recupera y devuelve el texto del encabezado de la página "Ayuda"
    public String validarLinkAyuda() {
        wait.until(ExpectedConditions.elementToBeClickable(linkAyuda)).click();
        wait.until(ExpectedConditions.visibilityOf(labelPaginaAyuda));
        return labelPaginaAyuda.getText();
    }

    //Método que recupera y devuelve el texto del encabezado de la página "Iniciar Sesión"
    public String validarBotonMiCuenta() {
        wait.until(ExpectedConditions.elementToBeClickable(botonMiCuenta)).click();
        wait.until(ExpectedConditions.visibilityOf(labelBotonMiCuenta));
        return labelBotonMiCuenta.getText();
    }

}
